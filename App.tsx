import React, {useState} from 'react';
import {
 ScrollView, View, ImageBackground
} from 'react-native';
import styles from './AppStyles';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import rootReducer from './src/store/reducers/rootReducer';
import Header from './src/components/header/Header';
import CardForcast from './src/components/card-forcast/CardForcast';
import CardPast from './src/components/card-past/CardPast';
import Footer from './src/components/footer/Footer';
const imageUp = require('./src/assets/images/bg-up.svg');
const imageDown = require('./src/assets/images/bg-down.svg');


const store = createStore(
  rootReducer,
  applyMiddleware(thunk)
);

const app: React.FC = (): JSX.Element => {
  return (
    <Provider store={store}>
    <ScrollView style={styles.wrapper}>
      <ImageBackground
        source={imageUp}
        style={styles.imageUp}
        resizeMode="stretch"
      ></ImageBackground>
      <ImageBackground
        source={imageDown}
        style={styles.imageDown}
        resizeMode="stretch"
      ></ImageBackground>
      <Header />
      <View style={styles.container}>
        <CardForcast />
        <CardPast />
      </View>
      <Footer />
    </ScrollView>
  </Provider>
  )
}

export default app;