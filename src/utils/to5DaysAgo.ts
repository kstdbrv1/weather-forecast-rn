const fiveDaysAgo: Date = new Date();
const oneDayOffset = 24 * 60 * 60 * 1000;

fiveDaysAgo.setTime(fiveDaysAgo.getTime() - oneDayOffset * 5);
export default fiveDaysAgo
