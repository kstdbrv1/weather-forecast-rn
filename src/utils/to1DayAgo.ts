const oneDayAgo: Date = new Date();
const oneDayOffset = 24 * 60 * 60 * 1000;

oneDayAgo.setTime(oneDayAgo.getTime() - oneDayOffset);
export default oneDayAgo;
