export const mainColor = '#2C2D76' as const;
export const secondColor = '#8083A4' as const;
export const bgColor = '#373AF5' as const;
export const mainFontSize = 16 as const;