import React, {useState} from 'react';
import { View } from 'react-native';
import type { PickerItem } from 'react-native-woodpicker';
import { Picker } from 'react-native-woodpicker';
import { CITIES } from '../../cities/cities';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { useActions } from '../../hooks/useActions';


const SelectCityPast: React.FC = () => {

  const { getLocation } = useActions();
  const cityLocation = useTypedSelector((state) => state.pastCardInfo.cityLocation);

  const setCityLocation = (e:any) => {
    /* eslint-disable */
    let longitude = '';
    let latitude = e.target.value;
    switch (latitude) {
     case '53.195873': // Самара
        return getLocation(latitude, longitude = '50.100193');
     case '53.507836': // Тольятти
        return getLocation(latitude, longitude = '49.420393');
     case '51.533557': // Саратов
        return getLocation(latitude, longitude = '46.034257');
     case '55.796127': // Казань
        return getLocation(latitude, longitude = '49.106405');
     case '45.035470': // Краснодар
        return getLocation(latitude, longitude = '38.975313');
     default: return;
    }
  }

  const cls = [
    'select-past__city',
    cityLocation === null ? null : 'select-past__city--active',
  ];
  const [pickedData, setPickedData] = useState<PickerItem>();
  
  const optionsList = CITIES.map(option => {
    return {
      label: option.name,
      value: option.lat
    }
  });

  const optionChanged = (pickerItem: PickerItem, index: number) => {
		const currentOption = CITIES[index];
		setPickedData(optionsList[index]);
		setCityLocation(currentOption.lat);
	};
      
  return (
    <View>    
      <Picker
				/* containerStyle={} */
				/* textInputStyle={{ width: "100%" }} */
				item={pickedData}
				items={optionsList}
				onItemChange={optionChanged}
				placeholder="Select city"
			/> 
    </View>
  )
}

export default SelectCityPast;