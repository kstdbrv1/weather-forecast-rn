import React, { useRef, useState } from 'react';
import { View, Text, Button, ScrollView } from 'react-native';
import styles from './CardForecastStyles';
import SelectCity from '../select-city/SelectCity';
import Loader from '../loader/Loader';
import UnitForecast from '../unit-forecast/UnitForecast';
import Placeholder from '../placeholder/Placeholder';
import { IForecastData } from '../../types/forecastData';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { useSelector } from 'react-redux';


const CardForcast: React.FC = () => {

  const { loading7DaysForecast } = useTypedSelector((state) => state.app);
  const forecast7DaysData = useSelector((state: any) => state.forecastData);

  const cardsToShow = useRef<number[]>([0, 1, 2]);
  const [currentShowCards, setCurrentShowCards] = useState<IForecastData[]>([]);

  const isInctiveNext:boolean = cardsToShow.current.includes(7);
  const isAIntivePrev:boolean = cardsToShow.current.includes(0);

  const setNextCards = () => {
    if (isInctiveNext) return;
    
    const nextCards = cardsToShow.current.map(card => {
      return card + 1
    });
    cardsToShow.current = nextCards;
    
    const currentShowCards: any = nextCards.map(card => {
      return forecast7DaysData.daily[card]
    });

    setCurrentShowCards(prev => currentShowCards);
  };

  const setPrevCards = () => {
    if (isAIntivePrev) return;
    
    const prevCards = cardsToShow.current.map(card => {
      return card - 1
    });
    cardsToShow.current = prevCards;

    const currentShowCards: any = prevCards.map(card => {
      return forecast7DaysData.daily[card]
    });

    setCurrentShowCards(prev => currentShowCards);
  };
  
  return (
    <View>
      <Text>
        7 Days Forecast
      </Text>
      <SelectCity forecast7DaysData={ forecast7DaysData } />
      {
        loading7DaysForecast ? <Loader /> :
        forecast7DaysData.daily ?
            <ScrollView horizontal={true}>
              <View>
              <UnitForecast currentShowCards={ currentShowCards } />
                <Button
                  disabled={isAIntivePrev ? true : false}
                  onPress={setPrevCards}
                  title='prev card'
                />
                <Button
                  disabled={isInctiveNext ? true : false}
                  onPress={setNextCards}
                  title='next card'
                />
              </View>
            </ScrollView>
            : <Placeholder />
      }
    </View>
  )
}

export default CardForcast;