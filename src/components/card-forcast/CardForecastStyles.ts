import { StyleSheet } from "react-native";
import { secondColor } from "../../variables";

const styles = StyleSheet.create({
  cardForcast: {
    flex: 1,
    alignItems: 'center',
    color: '#ffff',
    fontSize: 14,
    marginTop: 78,
    opacity: 0.6,
    lineHeight: 18,
    paddingBottom: 16,
  },
});

export default styles;