import React from 'react';
import { Text, View } from 'react-native';
import styles from './FooterStyles';

const Footer: React.FC = (): JSX.Element => (
  <View style={styles.footer}>
    <Text>C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT</Text>
  </View>
);

export default Footer;

