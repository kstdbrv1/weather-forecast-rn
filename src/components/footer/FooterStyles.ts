import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  footer: {
    flex: 1,
    alignItems: 'center',
    color: '#ffff',
    fontSize: 14,
    marginTop: 78,
    opacity: 0.6,
    lineHeight: 18,
    paddingBottom: 16,
  }
});

export default styles;