import React from 'react';
import { View, Text, Image } from 'react-native';
import { getDate } from '../../utils/getDate';
import { IPastData } from '../../types/pastData';
import { useSelector } from 'react-redux';
import styles from '../unit-past/UnitPastStyles';


interface IPastState {
  pastData:IPastData
};

const UnitPast:React.FC = () => {
  
  const pastDataForecast = useSelector((state: IPastState) => state.pastData);

  const hourResult = pastDataForecast.hourly[11]; // 11:00

  const date:string = getDate(pastDataForecast.current.dt);
  const temp:number = Math.round(hourResult.temp);

  return (
    <View>
      <Text>
        { date }
      </Text>
      {
        hourResult.weather.map(result => {

          let url = 'http://openweathermap.org/img/wn/' + result.icon + '@2x.png';
          return (
            <Image source={{uri: url}} />
          )
        })
      }
      <View>
        <Text>
        { temp >= 0 ? `+${temp}` : temp }
        </Text>
        <Text>°</Text>
      </View>
    </View>
  )
}

export default UnitPast;