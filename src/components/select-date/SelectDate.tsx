import React, { useState } from 'react';
import { View } from 'react-native';
import { DatePicker } from 'react-native-woodpicker';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import fiveDaysAgo from '../../utils/to5DaysAgo';
import oneDayAgo from '../../utils/to1DayAgo';
import { useActions } from '../../hooks/useActions';


const SelectDate:React.FC = () => {

  const { unixDate } = useTypedSelector((state) => state.pastCardInfo);

  const { getDate, fetchPastForecast } = useActions();

  const setDate = (e:any) => {

    const date = e.target.value;
    const unixDate = new Date(`${date}`).getTime() / 1000;

    if (unixDate > Date.now() / 1000) return;

    getDate(unixDate);
    fetchPastForecast();
  };

  const cls = [
    'select-past__date',
    unixDate === null ? null : 'select-past__date--active',
  ];

  const [pickedDate, setPickedDate] = useState<Date>();

  const textPicker = (): string => pickedDate
  ? pickedDate.toDateString()
    : 'Select date';
  
  const dateChanged = (selectedDate: Date) => {
		setPickedDate(selectedDate);
		setDate(selectedDate);
  } 

  return (
/*     <div
      className="select-past__wrapper"

    >
      <input
        className={ cls.join(' ').trim() }
        type="date"
        ref={ inputRef }
        onChange={setDate}
        min={fiveDaysAgo}
        max={oneDayAgo}
      />
    </div> */
        <View>
      <DatePicker
        	/* containerStyle={BaseSelectStyles.selectComponent__select} */
          /* textInputStyle={{ width: "100%" }} */
          iosMode="date"
          androidDisplay="calendar"
          value={pickedDate}
          onDateChange={dateChanged}
          text={textPicker()}
          minimumDate={fiveDaysAgo}
          maximumDate={oneDayAgo}
        />
      </View>
  )
}

export default SelectDate;