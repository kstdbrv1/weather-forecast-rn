import React from 'react';
import { View, Text, Button } from 'react-native';
import { useSelector } from 'react-redux';
import Loader from '../loader/Loader';
import SelectCityPast from '../select-city-past/selectCityPast';
import SelectDate from '../select-date/SelectDate';
import UnitPast from '../unit-past/UnitPast';
import Placeholder from '../placeholder/Placeholder'
import { IStateLoading } from '../../types/app';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { IPastData } from '../../types/pastData';
import styles from '../card-forcast/CardForecastStyles';


interface ILoading {
  app:IStateLoading
};

interface IPastState {
  pastData: IPastData
};

const CardPast: React.FC = () => {

  const { loadingPastForecast } = useTypedSelector((state:ILoading) => state.app);
  const pastDataForecast = useSelector((state:IPastState) => state.pastData);

  return (
    <View>
      <Text>
        Forecast for a Date in the Past
      </Text>
      <View>
        <SelectCityPast />
        <SelectDate />
      </View>
      {
        loadingPastForecast ? <Loader /> :
        pastDataForecast.hourly ?
        <UnitPast /> : <Placeholder />
      }
    </View>
  )
}

export default CardPast;