import React, { useState } from 'react';
import { View } from 'react-native';
import type { PickerItem } from 'react-native-woodpicker';
import { Picker } from 'react-native-woodpicker';
import { CITIES } from '../../cities/cities';
import { useActions } from '../../hooks/useActions';

const SelectCity = ({ forecast7DaysData }:any): JSX.Element => { 

  const { fetch7DayForecast } = useActions();

  const setCityLocation = (e:any) => {
    /* eslint-disable */
    let longitude = '';
    let latitude = e.target.value;
    switch (latitude) {
     case '53.195873': // Самара
      return fetch7DayForecast(latitude, longitude = '50.100193');
     case '53.507836': // Тольятти
      return fetch7DayForecast(latitude, longitude = '49.420393');
     case '51.533557': // Саратов
      return fetch7DayForecast(latitude, longitude = '46.034257');
     case '55.796127': // Казань
      return fetch7DayForecast(latitude, longitude = '49.106405');  
     case '45.035470': // Краснодар
      return fetch7DayForecast(latitude, longitude = '38.975313');
     default: return;
    }
  }
  
  let isEmptyForecast = Object.keys(forecast7DaysData);
  const cls = [
    'select-future__city',
    isEmptyForecast.length ? 'select-future__city--active' : null,
  ];

  const [pickedData, setPickedData] = useState<PickerItem>();
  
  const optionsList = CITIES.map(option => {
    return {
      label: option.name,
      value: option.lat
    }
  });

  const optionChanged = (pickerItem: PickerItem, index: number) => {
		const currentOption = CITIES[index];
		setPickedData(optionsList[index]);
		setCityLocation(currentOption.lat);
	};
      
  return (
    <View>    
      <Picker
				/* containerStyle={} */
				/* textInputStyle={{ width: "100%" }} */
				item={pickedData}
				items={optionsList}
				onItemChange={optionChanged}
				placeholder="Select city"
			/> 
    </View>
  )
}

export default SelectCity;