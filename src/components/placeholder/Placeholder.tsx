import React from 'react';
import { Text, Image } from 'react-native';
import styles from './PlaceholderStyles';
const image = require('../../assets/images/placeholder.svg');

const Placeholder: React.FC = () => {
  return (
    <React.Fragment>
      <Image
        source={image}
      />
      <Text style={styles.placeholderText}>
        Fill in all the fields and
        the weather will be displayed
      </Text>    
    </React.Fragment>
  )
}

export default Placeholder;