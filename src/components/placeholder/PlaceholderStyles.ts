import { StyleSheet } from "react-native";
import { secondColor } from "../../variables";

const styles = StyleSheet.create({
  placeholderText: {
    color: secondColor,
    lineHeight: 24,
    fontSize: 16
  }
});

export default styles;