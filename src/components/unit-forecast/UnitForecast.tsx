import { FC } from 'react';
import { View, Text, Image } from 'react-native';
import { getDate } from '../../utils/getDate';
import { IForecastData } from '../../types/forecastData';
import { useSelector } from 'react-redux';
import styles from './UnitForecastStyles';


interface IRootState {
  forecastData: {
    daily: IForecastData[]
  }
};

const UnitForecast: FC<any> = (props) => {
  
  let forecastData = useSelector((state: IRootState) => state.forecastData);
  let forecastDataToShow;

  if (window.screen.width > 768 && !!props.currentShowCards.length) {
      
    forecastDataToShow = props.currentShowCards;
  } else if (window.screen.width < 768) {
    
    forecastDataToShow = forecastData.daily;  
  } else {
    
      const cards:number[] = [0, 1, 2];
      forecastDataToShow = cards.map(card => {
        return forecastData.daily[card]
      }); 
    };
    
    return forecastDataToShow.map((day:any) => {
 
    const date:string = getDate(day.dt);
    const temp:number = Math.round(day.temp.eve);
    return (
        <View key={ day.dt }>
          <Text>
            { date }
          </Text>
        {
          day.weather.map((result: any) => {
            let url = 'http://openweathermap.org/img/wn/' + result.icon + '@2x.png';
            return (
              <Image source={{uri: url}} />
            )
          })
        }
          <View>
            <Text>
              { temp >= 0 ? `+${temp}` : temp }
            </Text>
            <Text>°</Text>
          </View>
        </View>
    )
  })
}

export default UnitForecast;