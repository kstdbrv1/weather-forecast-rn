import React from 'react';
import { Text } from 'react-native';

const Loader: React.FC = () => (
  <Text style={{ textAlign: 'center' }}>
    Loading data...
  </Text>
)

export default Loader;