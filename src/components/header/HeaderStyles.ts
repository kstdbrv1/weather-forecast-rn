import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  mainTitle: {
    marginTop: 45,
    marginBottom: 24,
    width: 260,
  },
  textLeft: {
    fontSize: 32,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'flex-start'
  },
  textRight: {
    fontSize: 32,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'flex-end'
  },
});

export default styles;