import React from 'react';
import { Text, View } from 'react-native';
import styles from './HeaderStyles';
import { useFonts } from 'expo-font';

const Header: React.FC = (): JSX.Element => {
  useFonts({
    'Ubuntu-Regular': require('../../assets/fonts/Ubuntu-Regular.ttf'),
    'Ubuntu-Bold': require('../../assets/fonts/Ubuntu-Bold.ttf'),
  });
  return (
    <View style={styles.container}>
     <View style={styles.mainTitle}>
      <Text style={styles.textLeft}>
        Weather
      </Text>
      <Text style={styles.textRight}>
        forecast
      </Text>
     </View>
    </View>
  )
};

export default Header;
