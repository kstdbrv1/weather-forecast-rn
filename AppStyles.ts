import { StyleSheet } from "react-native";
import { bgColor } from './src/variables';

const styles = StyleSheet.create({
  wrapper: {
   backgroundColor: bgColor,
  },
  container: {

  },
  imageUp: {
    flex: 1,
    position: 'absolute',
    right: 0,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageDown: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default styles;